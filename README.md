# Boilerplate for Parcel + Babel 7

This repository is a helpful staring point for using Parcel 1.x with Babel 7. It contains example browser target presets.

## Usage

To develop:

```sh
yarn
# or
yarn start
```

To develop with HMR:

```sh
yarn run watch
```

To build for production:

```sh
yarn build
```

### Watch limitations

Some filesystems do not properly support inotify which is the default method to watch for file changes. If this is the case, set the following variable in the same shell as you run yarn, just prior to starting / watching:

```sh
CHOKIDAR_USEPOLLING=1
```

### HMR limitations

Older browsers do not support websockets. This means they will not properly support HMR and may even throw a syntax error while in development mode. Please note this does not affect production builds.

To get around this, modify the start and/or watch scripts in `package.json` to include the `--no-hmr` flag.
